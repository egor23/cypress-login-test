/// <reference types="cypress" />

import Login from "../e2e/page_object/login"
import Menu from "../e2e/page_object/page_component/menu"
import CustomerSearch from "../e2e/page_object/customers_search"
import Customer from "../e2e/page_object/customer"
const loginpage = new Login()
const menu = new Menu()
const searchpage = new CustomerSearch()
const customer_page = new Customer()

describe('Search', () => {
  it(`Find customer by email`, () => {
    //Login
    cy.loginByAuth0Api("example@example.com", "12345678")
    cy.url().should('not.include', '/auth')
    //Navigate to customer page
    menu.clickCustomers()
    cy.url().should('include', '/customers')
    searchpage.findByEmail('test-user-sa-11@tabby.ai')
    cy.url().should('include', '/customers/')
  })
})

describe('Wallet modal', () => {
  it(`Compare amount`, () => {
    //Login
    cy.loginByAuth0Api("example@example.com", "12345678")
    customer_page.navigate('c0000000-0000-0000-0000-000000000011')
    cy.url().should('include', '/customers/')
    //Navigate to customer page
    customer_page.openWalletTab()
    cy.contains('Wallet amount').should('be.visible')
    customer_page.clickEditWallet()
    //cy.typeAndCheck('input[type="text"]', 100)

  })
})


describe('Wallet test', () => {
  it(`should contain validation error messages`, () => {
    //Login
    cy.loginByAuth0Api("example@example.com", "12345678")
    customer_page.navigate('c0000000-0000-0000-0000-000000000011')
    cy.url().should('include', '/customers/')
    //Open wallet edit modal
    customer_page.openWalletTab()
    cy.contains('Wallet amount').should('be.visible')
    
    customer_page.clickEditWallet()
    customer_page.confirmWalletEdit()
    cy.contains('Amount is a required field').should('be.visible')
    cy.contains('Pick a reason is a required field').should('be.visible')
    cy.contains('Add a note is a required field').should('be.visible')
    cy.contains('Ticket link is a required field').should('be.visible')
  })
})

describe('Legacy login and logout', () => {
  [
      "example@example.com",
      "collection_agent@example.com",
      "collection_manager@example.com",
      "sales_manager@example.com",
      "customer_agent@example.com",
      "finance_manager@example.com",
      "general_viewer@example.com",
      "finance_manager@example.com",
      "customer_viewer@example.com"
  ].forEach((login) => {
    it(`should be equal to ${login}`, () => {
      //Login
      loginpage.navigate()
      loginpage.typeEmail(login)
      loginpage.typePassword('12345678')
      loginpage.submit()
      cy.url().should('not.include', '/auth')
      //Check login
      menu.expand()
      cy.contains(login).should('be.visible')
      //Check logout
      menu.logOut()
      cy.url().should('include', '/auth')
    })
  })
})

