/// <reference types="cypress" />

class CustomerSearch{
    navigate(){
        cy.visit('/customers')
    }
    findByEmail(email){
        cy.typeAndCheck('input[name="email"]', email)
        cy.contains('li', email).click()
    }
    typePhone(phone_number){
        cy.typeAndCheck('input[name="phone"]', phone_number)
    }
    typePaymentId(payment_id){
        cy.typeAndCheck('input[name="email"]', payment_id)
    }
    typeNationalId(national_id){
    }
    submit(){
        cy.clickButtonByText('Log in')
    }
}

export default CustomerSearch