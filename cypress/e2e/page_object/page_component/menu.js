/// <reference types="cypress" />

class Menu{
    clickCustomers(){
        cy.clickByAreaLabel('Customers')
    }
    expand(){
        cy.clickByAreaLabel('Expand')
    }
    collapse(){
        cy.clickByAreaLabel('Collapse')
    }
    logOut(){
        cy.contains('Logout').click()
    }
}

export default Menu