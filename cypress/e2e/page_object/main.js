class MainPage{
    navigate(){
        cy.visit('/auth')
    }

    email(username){
        cy.get('input[name="email"]')
        .type(username)
        .should('have.value', username)
    }
    password(pw){
        cy.clickByAreaLabel('toggle password visibility')
        cy.get('input[name="password"]')
          .type(pw)
          .should('have.value', pw)
    }
    submit(){
        cy.get('button')
        .contains('Log in')
        .click()
    }
}

export default Login