/// <reference types="cypress" />

class Login{
    navigate(){
        cy.visit('/auth')
    }
    typeEmail(email){
        cy.typeAndCheck('input[name="email"]', email)
    }
    typePassword(pw){
        cy.clickByAreaLabel('toggle password visibility')
        cy.typeAndCheck('input[name="password"]', pw)
    }
    submit(){
        cy.clickButtonByText('Log in')
    }
}

export default Login