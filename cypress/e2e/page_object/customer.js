/// <reference types="cypress" />

class Customer{
    navigate(customer_id){
        cy.visit(`/customers/${customer_id}`)
    }
    openWalletTab(){
        cy.contains('button', /^Wallet$/).click()
    }
    clickEditWallet(){
        cy.contains('button', /^Edit$/).click()
    }
    confirmWalletEdit(){
        cy.clickButtonByText('Confirm')
    }
}

export default Customer