import * as jwt from "jsonwebtoken"

Cypress.Commands.add('clickByAreaLabel', (label_name) => {
    cy.get(`[aria-label="${label_name}"]`).click()
})
  
Cypress.Commands.add('typeAndCheck', (locator, value) => {
    cy.get(locator)
      .type(value)
      .should('have.value', value)
})

Cypress.Commands.add('clickButtonByText', (text) => {
    cy.contains('button', text).click()
})

Cypress.Commands.add(
  'loginByAuth0Api',
  (username, password) => {
    cy.log(`Logging in as ${username}`)
    
    cy.request({
      method: 'POST',
      url: '/api/v1/auth/login',
      body: {
        email: username,
        password: password
      },
    }).then(({ body }) => {
      const claims = jwt.decode(body.auth_token)
      const {
        customer_id,
        token_type,
        email,
        permissions,
      } = claims
      
      const item = {
        body: {
          ...body,
          decodedToken: {
            claims,
            user: {
              customer_id,
              token_type,
              email,
              permissions,
            },
          },
        },
      }
      
      window.localStorage.setItem('login', username)
      window.localStorage.setItem('tby:bo:token', body.auth_token)
      window.localStorage.setItem('permissions', JSON.stringify(claims.permissions))

      cy.visit('/')
    })
  }
)
