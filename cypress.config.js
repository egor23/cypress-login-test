const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: 'azn5fc',
  e2e: {
    baseUrl: 'https://backoffice.tabby.dev',
  },
});
